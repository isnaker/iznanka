<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('content');
            $table->integer('post_type')->default(0);
            $table->boolean('anonymity')->default(0);
            $table->integer('user_id')->nullable();
            $table->longText('images')->nullable();
            $table->integer('count_views')->default(0);
            $table->timestamp('publicated_at')->nullable();
            $table->timestamps();
        });

        Schema::create('post_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->required();
            $table->integer('category_id')->required();
            $table->timestamps();
        });

        Schema::create('post_city', function(Blueprint $table){
            $table->increments('id');
            $table->integer('post_id')->required();
            $table->integer('city_id')->required();
            $table->timestamps();
        });

        Shema::create('favorite_user_posts', function(Blueprint $table){
            $table->increments('id');
            $table->integer('post_id');
            $table->integer('user_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
