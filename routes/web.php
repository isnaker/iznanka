<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'web'], function(){


    Auth::routes();

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');



    Route::group([
        'prefix'     => 'profile',
        'middleware' => ['auth'],
        'namespace'  => 'Profile',

    ],function(){

        Route::get('/', 'ProfileController@index');
        Route::get('edit', 'ProfileController@edit');
        Route::get('favorite', 'ProfileController@favorite');
        Route::post('update', 'ProfileController@update');
    });


//Route's for COMMENT ----------------------------------|
    Route::group([
        'namespace'=>'Comment',
        'prefix'  => 'comment'
    ], function (){

        Route::post('/add/{postId}', 'CommentController@create');
    });

//Route's for Category ----------------------------------|
    Route::group([
        'namespace'=>'Category'
    ], function (){

        Route::get('/{city}/{category}/{subcat?}', 'CategoryController@index');
    });
//END Route's for CATEGORY ------------------------------|

//Route's for POST ----------------------------------|
    Route::group([
        'namespace'=>'Posts'
    ], function (){

        Route::post('/post/favorite/add/ajax/{id}', 'PostsController@addToFavorite');
        Route::get('/post/get/ajax/{id}', 'PostsController@get');
        Route::post('/posts/like/add/{postId}', 'PostsController@addLike');
        Route::get('/{city}/{category}/post/create', 'PostsController@create');
        Route::post('/{city}/{category}/post/save', 'PostsController@save');

    });
//END Route's for POST ------------------------------|


/*
//Route's for CITY ----------------------------------|
    Route::group([
        'namespace'=>'City'
    ], function (){

        Route::get('/{city}/{category}', 'PostsController@index');
    });
//END Route's for CITY ------------------------------|


//Route's for CATEGORY ----------------------------------|
    Route::group([
        'namespace'=>'Category'
    ], function (){

        Route::get('/{city}/{category}', 'PostsController@index');
    });
//END Route's for CATEGORY ------------------------------|

*/
//delete login and register route for admin  ------------------------------|
    Route::get('admin/register', function(){
        abort(403, 'Access Denied');
    });

    Route::get('admin/login', function(){
        abort(403, 'Access Denied');
    });
//-------------------------------------------------------------------------|


});
