<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('cities', 'CitiesCrudController');
    CRUD::resource('category','CategoryCrudController');

    CRUD::resource('posts','PostsCrudController');
    Route::post('/posts/{id}/upload_images ','PostsCrudController@ajaxUploadImages');
    Route::post('/posts/{id}/reorder_images ','PostsCrudController@ajaxReorderImages');
    Route::post('/posts/{id}/delete_image ','PostsCrudController@ajaxDeleteImage');



}); // this should be the absolute last line of this file
