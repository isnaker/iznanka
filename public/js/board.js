// add comment
$(document).ready(function() {
    $('.sendComment').submit(function (e) {
        e.preventDefault();
        var postId = $(this).children('#post_id').val();
        var message = $(this).find('#exampleFormControlTextarea1').val();
        var tok   = $(this).children('input[name=_token]').val();
        $.ajax({
            type: 'POST',
            url: '/comment/add/'+postId,
            data: {message: message, _token:tok},
            success: function (responce) {

                if (responce.status === 'success'){
                    $('#pills-comments-'+postId).append('' +
                        '<div class="media">' +
                        '<img width="65" height="65" src="http://board/storage/profile/81db36a6c63d657110621ed4f8b60e56.jpg" class="mr-3">' +
                        ' <div class="media-body">' +
                        '<h5 class="mt-0">Admin</h5>\n' +
                        responce.data.message +
                        '</div>' +
                        '</div>');
                }
            }
        });
    });
});

// get post
$(document).ready(function () {
    console.log('document was loadled get post');
    // get POST
    $('.gettingPost').click(function (getPost) {
        console.log('click get post');
       // var tok   = $('input[name=_token]').val();
        getPost.stopPropagation();
        getPost.preventDefault();
        var postId = $(this).children('input[name=post_id]').val();
        $.ajax({
            type: 'GET',
            url : '/post/get/ajax/'+postId,
            success: function (responce) {
                if (responce.status === 'success')
                {
                    console.log(responce.data);
                }
            }

        });

        return false;
    });
});

// add post TO favorite
$(document).ready(function () {
    console.log('document was loadled get post');
    //  add post TO favorite
    $('.addToFavorite').click(function (getPost) {
        console.log('click add post to favorite');
        // var tok   = $('input[name=_token]').val();
        getPost.stopPropagation();
        getPost.preventDefault();
        var postId = $(this).children('input[name=post_id]').val();
        var tok   = $(this).children('input[name=_token]').val();
        $.ajax({
            type: 'POST',
            url : '/post/favorite/add/ajax/'+postId,
            data: {_token:tok},
            success: function (responce) {
                if (responce.status === 'success')
                {
                    $('.favoritePost-'+postId).html('Добавить в избранное : Пост уже в избранном');
                }
            }

        });

        return false;
    });
});

//like add PLUS for POST, if you want add like to Comment, you must set type as Comment

$(document).ready(function() {
    console.log('document was loadled');

    $('.plusLike').click(function (clickPlus) {
        console.log('clickPlus');
        var tok   = $('input[name=_token]').val();

        clickPlus.stopPropagation();
        clickPlus.preventDefault();
        var postId = $(this).children('input[name=post_id]').val();
        $.ajax({
            type: 'POST',
            url : '/posts/like/add/'+postId,
            data: {like: 1, type: 'Posts', _token:tok},
            success: function (responce) {
                if (responce.status === 'success')
                {
                    $('#pills-home-'+postId+' .countLike').html(responce.data);
                }
            }
            
        });

        return false;
    });


});

// like add MINUS for POST, if you want add like to Comment, you must set type as Comment
$(document).ready(function() {
    console.log('document was loadled');

    $('.minusLike').click(function (clickMinus) {
        console.log('clickPlus');
        var tok   = $('input[name=_token]').val();

        clickMinus.stopPropagation();
        clickMinus.preventDefault();
        var postId = $(this).children('input[name=post_id]').val();
        $.ajax({
            type: 'POST',
            url : '/posts/like/add/'+postId,
            data: {like: -1, type: 'Posts', _token:tok},
            success: function (responce) {
                if (responce.status === 'success')
                {
                    $('#pills-home-'+postId+' .countLike').html(responce.data);
                }
            }

        });

        return false;
    });

});