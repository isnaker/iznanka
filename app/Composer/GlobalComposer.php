<?php
/**
 * Created by PhpStorm.
 * User: houme
 * Date: 14.06.2018
 * Time: 14:45
 */

namespace App\Composer;

use App\Traits\CityHandler;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use Request;


class GlobalComposer
{
    use CityHandler;

    public function compose (View $view){


        $view->with('definedCity', $this->defineCity($_SERVER['REMOTE_ADDR']));
        $view->with('categories', Category::where('visible', '=', 1)->where('parent_id', '=', NULL)->with('subcat')->get());
    }
}