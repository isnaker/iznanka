<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'comments';

    protected $fillable = [
        'user_id','post_id','message'
    ];


    public function post(){
        return $this->belongsTo('App\Models\Posts', 'post_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id', 'id');
    }

    public function likes (){
        return $this->morphMany('App\Models\Like', 'liked');
    }
}