<?php

namespace App\Models;

use App\City;
use Illuminate\Database\Eloquent\Model;

use Backpack\CRUD\CrudTrait;

class Posts extends Model
{

    use CrudTrait;


    //public $images;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $casts = [
        'images'=>'array'
    ];
    protected $fillable = ['title','publicated_at','content','post_type','images','user_id','anonymity', 'count_views'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function updateImageOrder($order) {
        $new_images_attribute = [];

        foreach ($order as $key => $image) {
            $new_images_attribute[$image['id']] = $image['path'];
        }
        $new_images_attribute = json_encode($new_images_attribute);

        $this->attributes['images'] = $new_images_attribute;
        $this->save();
    }

    public function removeImage($image_id, $image_path, $disk)
    {
        // delete the image from the db
        $images = json_encode(array_except($this->images, [$image_id]));
        $this->attributes['images'] = $images;
        $this->save();

        // delete the image from the folder
        if ( \Storage::disk($disk)->has($image_path)) {
             \Storage::disk($disk)->delete($image_path);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category()
    {
        return $this->belongsToMany(Category::class, 'post_category', 'post_id', 'category_id');
    }


    public function city(){
        return $this->belongsToMany(Cities::class, 'post_city', 'post_id', 'city_id');
    }

    public function comments(){
        return $this->hasMany(Comment::class, 'post_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }

    public function likes(){
        return $this->morphMany('App\Models\Like', 'liked');
    }

    public function isFavorite(){
        $user = auth()->user();

         return $this->belongsTo('App\Models\FavoritePost','id','post_id')
             ->where('user_id', ($user)?$user->id:0);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeVisible($query){
        $query->where('post_type', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setContentAttribute($value){
        $this->attributes['content'] = strip_tags($value);
    }

    public function setTitleAttribute($value){
        $this->attributes['title'] = strip_tags($value);
    }

    public function setImagesAttribute($value)
    {
        $attribute_name = "images";
        $disk = 'public';
        $destination_path = "uploads";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function setUserIdAttribute()
    {
        if ($user = auth()->user()){
            $this->attributes['user_id'] = $user->id;
        }else{
            $this->attributes['user_id'] = null;
        }

    }
}
