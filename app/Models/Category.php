<?php

namespace App\Models;

use App\Traits\CityHandler;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
    use CrudTrait;
    use CityHandler;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'category';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['parent_id', 'name', 'description','visible','uri'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parent ()
    {
        return $this->hasOne('App\Models\Category', 'id','id');
    }

    public function subcat(){
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')
            ->where('visible','=',1);
    }

    public function parental (){
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    /*
     * Posts with cities
     */
    public function posts($city){

        return $this->belongsToMany('App\Models\Posts', 'post_category',
            'category_id', 'post_id')->whereHas('city', function ($query) use ($city){
                $query->where('cities.uri', $city );
        });
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeVisible($query){
        return $query->where('visible', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setParentIdAttribute($value)
    {
        // unset field if user set parent category as it self
        if (isset($this->attributes['id']) && $this->attributes['id'] == $value){
            $this->attributes['parent_id'] == null;
        }else{
            $this->attributes['parent_id'] = $value;
        }

    }
}
