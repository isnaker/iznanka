<?php
/**
 * Created by PhpStorm.
 * User: houme
 * Date: 28.06.2018
 * Time: 22:58
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class FavoritePost extends Model
{
    protected $table = 'favorite_user_posts';

    protected $fillable = ['post_id', 'user_id'];

}