<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

    protected $table = 'likes';

    protected $fillable = ['like', 'user_id', 'liked_id', 'liked_type'];

    public function liked (){
        return $this->morphTo();
    }
}