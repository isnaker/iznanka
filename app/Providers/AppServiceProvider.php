<?php

namespace App\Providers;

use App\Models\Cities;
use App\Traits\CityHandler;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    use CityHandler;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $_SERVER['REMOTE_ADDR'] = '217.118.8eff1.17';

        view()->share('cities', Cities::all()->where('visible', '=', 1));

        Relation::morphMap([
            'Posts' => 'App\Models\Posts',
            'Comment'=>'App\Models\Comment'
        ]);

       // view()->share('definedCity',$this->defineCity($_SERVER['REMOTE_ADDR']));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
