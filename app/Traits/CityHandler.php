<?php
/**
 * Created by PhpStorm.
 * User: houme
 * Date: 14.06.2018
 * Time: 10:05
 */

namespace App\Traits;
use App\City;
use Illuminate\Support\Facades\Auth;
use Request;


trait CityHandler
{

    public function defineCity($ip){

        //define city from uri path
        $cityFromPath = City::where('visible','=',1)->where('uri', '=',Request::segment(1) )->first();

        if (count($cityFromPath) > 0){
            return $cityFromPath;
        }

        // define city from user settings
        $user = auth()->user();

        if ($user){

            $user = $user->load('city');

                if (count($user->city) != 0){
                    return $user->city;
                }
        }

        // define city from remote_addr ($_Server)
        $geoInformation = geoip($ip);

        $city = City::where('name','=',$geoInformation->city)->first();

        if (count($city) != 0){
            return $city;
        }

        $geoInformation->name = $geoInformation->city;
        return $geoInformation;
    }

}