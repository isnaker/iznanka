<?php

namespace App;

use Backpack\CRUD\CrudTrait; //
use Spatie\Permission\Traits\HasRoles;//
use Illuminate\Notifications\Notifiable;
use App\Models\Cities;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;
    use CrudTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','images','city_id',
    ];

    protected $casts = [
        'images'=>'array'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function city (){
        return $this->hasOne(Cities::class, 'id', 'city_id');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment', 'user_id','id');
    }

    public function posts(){
       return $this->hasMany('App\Models\Posts', 'user_id','id');
    }

    public function favoritePosts(){
        return $this->belongsToMany('App\Models\Posts', 'favorite_user_posts','user_id','post_id');
    }

    public function setImagesAttribute($value)
    {
        $attribute_name = "images";
        $disk = 'public';
        $destination_path = "profile";
        $this->images = null;
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
