<?php

namespace App\Listeners;

use App\Events\PostViewed;
use App\Models\Posts;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventPostViewed
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
     //
    }

    /**
     * Handle the event.
     *
     * @param  PostViewed  $event
     * @return void
     */
    public function handle(PostViewed $event)
    {
        $event->post->count_views += 1;
        $event->post->save();
    }
}
