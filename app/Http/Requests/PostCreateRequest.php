<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;


class PostCreateRequest extends FormRequest
{

    public function authorize(){
        return \Auth::check();
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'title'  => 'string|required',
            'content'=> 'required',
            'city'   => 'array|size:1',
            'city.*' => 'exists:cities,id',
            'category'   => 'array|size:1',
            'category.*' => 'exists:category,id',
            'images.*' => 'image',
            'anonymity' => 'integer'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title',
            'content',
            'city',
            'category',
            'anonymity',
            'images'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'city.size'=>'Система позволяет добавить несколько городов для одного поста, но в настоящий момент данная функция отключена.',
        ];
    }
}