<?php


namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /*
     * @param $city - param from path, will be query for fetching posts
     * @param $category - param from path, will be query for fetching posts
     *
     *
     * @return posts with pagination and category
     */
    public function index($city, $category, $subcat = null){

        $cat = $subcat ?: $category;
        $category = Category::where('uri','=',$cat)->first();

        if (!$category)
            return view('errors.404');

        $posts = $category->posts($city)
            ->visible()
            ->with('comments.user')
            ->orderBy('created_at', 'DESC')
            ->paginate(env('MAX_POST_PER_PAGE'));

        return view('category.category', compact('category', 'posts'));
    }
}