<?php

namespace App\Http\Controllers\Posts;

use App\City;
use App\Events\PostViewed;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostCreateRequest;
use App\Models\Category;
use App\Models\FavoritePost;
use App\Models\Like;
use Illuminate\Http\Request;
use App\Models\Posts as Posts;
use Illuminate\Support\Facades\Event;

class PostsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth')->except('get');
    }

    /*
     * @param $city - param from path, will be query for fetching city
     * @param $category - param from path, will be query for fetching category
     *
     */
    public function create($city, $category){

        $city     = City::where('uri','=', $city)->firstOrFail();
        $category = Category::where('uri','=', $category)->firstOrFail();

        return view('posts.create', compact('city', 'category'));

    }

    /*
     * @param PostCreateRequest
     *
     * Save post
     */
    public function save(PostCreateRequest $request){

        $data = $request->validated();
        $data['user_id'] = true;

        // get city and category
        $city           = City::where('id', $data['city'][0])->firstOrFail();
        $category       = Category::where('id', $data['category'][0])->with('parental')->firstOrFail();
        $uriForRedirect = ($category->parental == null) ? $city->uri.'/'.$category->uri
                                                        : $city->uri.'/'.$category->parental->uri.'/'.$category->uri;

        // save post with relations
         $post = Posts::create($data);
         $post->category()->attach($data['category']);
         $post->city()->attach($data['city']);

         return \Redirect::to($uriForRedirect)->with('status', 'Ваш пост был отправлен на проверку, после одобрения Администратором, будет автоматически добавлен в ленту');
    }


    /*
     * ajax method for adding like
     *
     *
     * @postId - it is id for searching entry
     * @type   - it is Model name.
     */
    public function addLike($postId, Request $request){
        $user           = auth()->user();
        $validatedData  = $request->validate(['like'=>'required|in:-1,1', 'type'=>'required|in:Posts,Comment']);

        // create path to model-obj-------------------------------#|
        $className      = 'App\Models\\'.$validatedData['type'];#|
        $className      = new $className();                     #|
        //------------------------------------------------------#|

        $post           = $className->findOrFail($postId);


        // checking like, if exist return error;
        $isAdded  = Like::where('liked_id', $postId)
            ->where('user_id', $user->id)
            ->where('liked_type','=','Posts')
            ->where('like', '=', $validatedData['like'])
            ->first()
        ;
        if ($isAdded)
            return response()->json(['status'=>'error', 'message'=>'Вы не можете оценивать повторно']);

        $isAddedReverse = Like::where('liked_id', $postId)
            ->where('user_id', $user->id)
            ->where('liked_type','=','Posts')
            ->where('like', '=', ($validatedData['like'] == 1) ? -1 : 1 )
            ->first()
        ;
        if ($isAddedReverse)
            $isAddedReverse->delete();

        
        //adding like
        $post->likes()->create([
            'like'=>$validatedData['like'],
            'user_id'=>$user->id,
            'liked_id'=>$postId,
            'liked_type'=>$validatedData['type']
        ]);

        return response()->json([
            'status'=>'success',
            'message'=>'Оценка учтена',
            'data'=>$post->likes()->sum('like')
        ]);

    }


    public function get($postId){

        $post = Posts::where('id', $postId)->with('comments.user')->firstOrFail();

        $response = Event::fire(new PostViewed($post));

        return response()->json(['status'=>'success', 'message'=>'Пост был получен', 'data'=>$post]);
    }


    public function addToFavorite($postId){

        $user                  = auth()->user();
        $postIsAddedToFavorite = FavoritePost::where('post_id', $postId)->where('user_id', $user->id)->first();
        $ifPostExist           = Posts::where('id', $postId)->visible()->firstOrFail();

        if ($postIsAddedToFavorite)
            return response()->json(['status'=>'error', 'message'=>'Запись была добавлена ранее']);

        $user->favoritePosts()->attach(['post_id'=>$postId]);

        return response()->json(['status'=>'success','message'=>'Добавлено в избраное']);
    }
}