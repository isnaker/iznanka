<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PostsRequest as StoreRequest;
use App\Http\Controllers\Admin\Traits\AjaxUploadImagesTrait;
use App\Http\Requests\PostsRequest as UpdateRequest;

class PostsCrudController extends CrudController
{
    use AjaxUploadImagesTrait;
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Posts');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/posts');
        $this->crud->setEntityNameStrings('posts', 'posts');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        /*
        |------------------------------------------------------------|
        | Filter for published data
        |------------------------------------------------------------|
         */
        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Status'
        ], [
            0 => 'Unpublished',
            1 => 'Published',
        ], function($value) { // if the filter is active
             $this->crud->addClause('where', 'post_type', $value);
        });





        // ------ CRUD FIELDS
        //title
        $this->crud->addField([
            'name' => 'title',
            'type' => 'text',
            'label' => 'Title',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ]);

        // Published date
        $this->crud->addField([
            'name' => 'publicated_at',
            'type' => 'datetime',
            'label' => 'Publicated at',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ])->afterField('title');

        $this->crud->addField([
            'label' => "Category",
            'type' => 'select2_multiple',
            'name' => 'category', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Category", // foreign key model
            'pivot'=>true,
            'wrapperAttributes'=>[
                'class'=>'form-group col-md-6'
            ],

        ])->afterField('publicated_at');


        $this->crud->addField([
            'label' => "City",
            'type' => 'select2_multiple',
            'name' => 'city', // the db column for the foreign key
            'entity' => 'city', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Cities", // foreign key model
            'pivot'=>true,
            'wrapperAttributes'=>[
                'class'=>'form-group col-md-6'
            ],

        ])->afterField('category');


        $this->crud->addField([
            'name'          => 'images',
            'type'          => 'dropzone',
            'upload_route'  => 'upload_images',
            'reorder_route' => 'reorder_images',
            'delete_route'  => 'delete_image',
            'disk'          => 'public', // local disk where images will be uploaded
            'mimes'         => 'image/*', //allowed mime types separated by comma. eg. image/*, application/*, etc
            'filesize'      => 5, // maximum file size in MB
        ], 'update')->beforeField('post_type');

        $this->crud->addField([
            'name'  => 'post_type',
            'label' => "Confirmed",
            'type'  => 'select_from_array',
            'options' => [0=>'false',1=>'true'],// Child model entity related to active model (belongsTo) (as per normal use)
            //'default' => '1',
        ]);

        $this->crud->addField([
            // Hidden
            'name' => 'user_id',
            'type' => 'hidden'
        ]);

        $this->crud->addField([
               // Checkbox
                'name' => 'anonymity',
                'label' => 'Anonymity',
                'type' => 'checkbox',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'style' => 'margin-left:80%'

            ]
        ]);

        // $this->crud->addFields($array_of_arrays, 'update/create/both');
         $this->crud->removeField('images', 'create');
         //$this->crud->removeField('user_id');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
         //$this->crud->addColumn('fddf'); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
         $this->crud->removeColumn('images'); // remove a column from the stack
         $this->crud->removeColumn('user_id'); // remove a column from the stack
         $this->crud->removeColumn('post_type');
        // $this->crud->removeColumn('anonymity'); // remove a column from the stack// remove a column from the stack
       // $this->crud->removeColumn('publicated_at');
        $this->crud->removeColumn('content');

        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
