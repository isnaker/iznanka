<?
namespace App\Http\Controllers\Admin\Traits;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

trait AjaxUploadImagesTrait {

/**
* Upload an image with AJAX to the disk
* and store its path in the database.
*
* @param  Request $request [description]
* @return [type]           [description]
*/
public function ajaxUploadImages(Request $request)
{

    $entry = $this->crud->getEntry($request->input('id'));
    $attribute_name = 'images';
    $files = $request->file($attribute_name);
    //print_R($files);
   // print_r($files); exit;
    $file_count = count($files);

    //var_dump($attribute_name);
    $entry->{$attribute_name} = $files;

    if (count($entry->{$attribute_name}) >= 10)
        return response()->json(['success'=>false,'message'=>'Количество изображений не должно превышать 10', 'images' => $entry->{$attribute_name}]);

    $entry->save();

    return response()->json([
        'success' => true,
        'message' => ($file_count>1)?'Uploaded '.$file_count.' images.':'Image uploaded',
        'images' => $entry->{$attribute_name}
    ]);
}

/**
* Save new images order from sortable object.
*
* @param  Request $request [description]
* @return [type]           [description]
*/
public function ajaxReorderImages(Request $request)
{
    $entry = $this->crud->getEntry($request->input('entry_id'));
    $entry->updateImageOrder($request->input('order'));

    return response()->json([
        'success' => true,
        'message' => 'New image order saved.'
    ]);
}

/**
* Delete an image from the database and disk.
*
* @param  Request $request [description]
* @return [type]           [description]
*/
public function ajaxDeleteImage(Request $request)
{
    $image_id = $request->input('image_id');
    $image_path = $request->input('image_path');
    $entry = $this->crud->getEntry($request->input('entry_id'));
    $disk = $this->crud->getFields('update', $entry->id)['images']['disk'];

    // delete the image from the db
    $entry->removeImage($image_id, $image_path, $disk);

    return response()->json([
        'success' => true,
        'message' => 'Image deleted.'
    ]);
}

}