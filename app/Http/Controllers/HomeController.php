<?php

namespace App\Http\Controllers;

use App\Traits\CityHandler;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use CityHandler;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->defineCity($_SERVER['REMOTE_ADDR']);
        return view('home');
    }
}
