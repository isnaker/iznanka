<?php

namespace App\Http\Controllers\Comment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posts;

class CommentController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /*
     * Method for create comment through ajax
     *
     */
    public function create($postId, Request $request){
        $post = Posts::findOrFail($postId);
        $user = auth()->user();

        if ($request->input('message') == NULL)
            return response()->json(['status'=>'error', 'message'=>'Сообщение не может быть пустым']);

        $comment = $post->comments()->create([
            'post_id'=>$postId,
            'user_id'=>$user->id,
            'message'=>$request->input('message')
        ]);

        return response()->json([
            'status'=>'success',
            'message'=>'Ваш комментарий добавлен',
            'data' => $comment
        ]);
    }
}