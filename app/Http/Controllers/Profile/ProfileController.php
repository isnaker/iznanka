<?php


namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ProfileController  extends Controller
{


    /*
     * Show user dashboard
     *
     */
    public function index(){
       $user = auth()->user();
       $user->load('city');

       return view('profile.profile', ['user'=>$user]);
    }

    public function edit (){

      $user = auth()->user();
      $user->load('city');

        return view('profile.edit', ['user'=>$user->toArray()]);
    }

    public function update(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'city_id'  => 'required|integer',
            'phone'    => 'regex:/[0-9]{6,12}/',
            'images'   => ''
        ]);

       foreach ($validatedData as $attribute=>$value)
       {
           $user->{$attribute} = $value;
       }

       $user->save();

       return redirect('profile');
    }
    /*
     * Favorite posts
     */
    public function favorite(){

        $user = auth()->user();

        return view('profile.favorite', ['posts'=>$user->favoritePosts]);
    }
}