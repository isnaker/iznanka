<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();

        if (!$user) return $next($request);

        if (!$user->hasRole('Admin'))
        {
           // auth()->logout();
            abort(403, 'Access denied');
        }

        return $next($request);
    }
}
