

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">

                @foreach($categories as $category)


                    @if (count($category->subcat) > 0)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="{{url($definedCity->uri.'/'.$category->uri)}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               {{$category->name}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                @foreach($category->subcat as $subcat)
                                    <a class="dropdown-item" href="{{url($definedCity->uri.'/'.$category->uri.'/'.$subcat->uri)}}">{{$subcat->name}}</a>
                                @endforeach
                            </div>
                        </li>

                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{url($definedCity->uri.'/'.$category->uri)}}">{{$category->name}}</a>
                        </li>
                    @endif

                @endforeach

            </ul>
        </div>
    </nav>
</div>