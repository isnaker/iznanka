@extends('layouts.app')

@section('title', 'Лента '.$category->name)
@section('description', 'Описание')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$category->name}}
                        <a href="{{url($definedCity->uri.'/'.$category->uri.'/post/create')}}"
                           style="margin-left: 70%;">Добавить</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{-- Посты --}}
                        @if (count($posts) > 0)
                            @foreach($posts as $post)
                                <div class="card mb-3">
                                    {{-- Картинки поста --}}
                                    @if (count($post->images ) > 0)
                                        @foreach($post->images as $image)
                                            <img class="card-img-top" height="250" src="{{url('storage/'.$image)}}"
                                                 alt="Card image cap">
                                            @break
                                        @endforeach

                                    @endif

                                    <div class="card-body">
                                        <h5 class="card-title">{{$post->title}}</h5>
                                        <p class="card-text">{{$post->content}}
                                            <a class="gettingPost badge badge-info" href="#" >Подробнее<input type="hidden" name="post_id" value="{{$post->id}}">
                                            </a>
                                        </p>

                                        <ul class="nav nav-pills mb-3" id="pills-tab-{{$post->id}}" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pills-home-tab-{{$post->id}}" data-toggle="pill"
                                                   href="#pills-home-{{$post->id}}" role="tab" aria-controls="pills-home"
                                                   aria-selected="true">Основное</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-comments-tab-{{$post->id}}" data-toggle="pill"
                                                   href="#pills-comments-{{$post->id}}" role="tab" aria-controls="pills-profile"
                                                   aria-selected="false">Комментарии</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="pills-home-{{$post->id}}" role="tabpanel"
                                                 aria-labelledby="pills-home-tab-{{$post->id}}">
                                                <p class="card-text">
                                                    <small class="text-muted">
                                                        Добавлен {{intval((strtotime(date('Y-m-d H:i:s'))-strtotime($post->created_at))/60)}}
                                                        мин назад
                                                    </small>
                                                </p>
                                                <p class="card-text">
                                                    <small class="text-muted">
                                                        Комментарии: {{count($post->comments)}}</small>
                                                </p>
                                                <p class="card-text">
                                                    <small class="text-muted">
                                                        Пользователь: {{$post->user->name}}</small>
                                                </p>
                                                <p class="card-text">
                                                    <small class="text-muted">
                                                        Просмотры: {{$post->count_views}}</small>
                                                </p>

                                                <p class="card-text">
                                                    <small class="text-muted">
                                                        Лайки: <span class="countLike">{{$post->likes->sum('like')}}</span>
                                                    </small>
                                                    <small class="like">
                                                        @csrf
                                                        <a href="#"  class="plusLike badge badge-success">Понравилось<input type="hidden" name="post_id" value="{{$post->id}}"></a>
                                                         <a href="#" class="minusLike badge badge-danger">Не понравилось<input type="hidden" name="post_id" value="{{$post->id}}"></a>
                                                    </small>
                                                </p>

                                                @if (!$post->isFavorite)
                                                <p class="card-text">
                                                    <small class="favoritePost-{{$post->id}} text-muted">
                                                        Добавить в избранное :
                                                        <a href="#" class="addToFavorite badge badge-info">В избранное @csrf <input type="hidden" name="post_id" value="{{$post->id}}"></a>
                                                    </small>
                                                </p>

                                                 @else
                                                    <p class="card-text">
                                                        <small class="favoritePost-{{$post->id}} text-muted">
                                                            Добавить в избранное : Пост уже в избранном
                                                        </small>
                                                    </p>
                                                @endif
                                            </div>

                                            <div class="tab-pane fade" id="pills-comments-{{$post->id}}" role="tabpanel"
                                                 aria-labelledby="pills-comments-tab-{{$post->id}}">
                                                {{-- Комментарии --}}
                                                @foreach($post->comments as $comment)
                                                    <div class="media">
                                                        <img class="mr-3" width="65" height="65"
                                                             src="{{url('storage/'.$comment->user->images[0])}}">
                                                        <div class="media-body">
                                                            <h5 class="mt-0">{{$comment->user->name}}</h5>
                                                            {{$comment->message}}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                            @auth
                                                <div class="addComment">
                                                    <form class="sendComment">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlTextarea1">Добавить комментарий</label>
                                                            <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                        </div>
                                                        @csrf
                                                        <input type="hidden" id="post_id" name="post_id" value="{{$post->id}}">
                                                        <button type="submit" class="btn btn-primary">Отправить</button>
                                                    </form>
                                                </div>
                                            @endauth
                                        </div>


                                    </div>
                                </div>
                            @endforeach

                        @else
                            Ничего не найдено
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection