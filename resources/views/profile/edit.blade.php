@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                    <form method="post" action="{{url('profile/update')}}" enctype="multipart/form-data">
                        @csrf
                            <div class="form-group">
                                <label for="disabledTextInput">Имя</label>
                                <input type="text" id="disabledTextInput" name="name" class="form-control" value="{{$user['name']}}">

                            </div>
                            <div class="form-group">
                                <label for="disabledTextInput">Email</label>
                                <input type="text" name="email" class="form-control" value="{{$user['email']}}">

                            </div>

                            <div class="form-group">
                                <label for="disabledTextInput">Телефон</label>
                                <input type="text" name="phone" id="disabledTextInput" class="form-control" value="{{$user['phone']}}">

                            </div>


                            <div class="form-group">
                                <label for="disabledTextInput">Город</label>
                               @include('cities.part.cities_select')
                            </div>

                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Фото</label>
                                    <input type="file" name="images[]" class="form-control-file" id="exampleFormControlFile1">

                                </div>

                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection