@extends('layouts.app')

@section('title', 'Профиль')
@section('description', 'Описание')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль  <a style="margin-left: 70%;" href="{{url('profile/edit')}}">Редактировать</a></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                                <fieldset disabled>

                                    <div class="form-group">
                                        <label for="disabledTextInput">Имя</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="{{$user['name']}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Email</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="{{$user['email']}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Телефон</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="{{$user['phone']}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Город</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="{{$user['city']['name']}}">
                                    </div>
                                    @if($user['images'] != null)
                                        @foreach($user['images'] as $val)
                                            <div class="form-group">
                                                <label for="disabledTextInput">Фото</label>
                                                <img src="{{url('storage/'.$val)}}" alt="фото профиля" class="img-thumbnail">
                                            </div>
                                        @endforeach
                                    @endif

                                </fieldset>

                        {{print_R($user->favoritePosts->toArray())}}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection