
<select class="custom-select" style="width: 10%;" name="city_id">
    <option value="0">{{__('Выберите город')}}</option>
    @foreach($cities as $city)

      @if($city->name == $definedCity->name)
         <option value="{{$city->id}}" selected>{{$city->name}}</option>
      @else
         <option value="{{$city->id}}">{{$city->name}}</option>
      @endif

  @endforeach
</select>