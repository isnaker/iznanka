@extends('layouts.app')

@section('title', 'Лента '.$category->name)
@section('description', 'Описание')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Создание нового поста в ленте - {{$category->name}} </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($errors->any())
                           <div class="alert alert-danger">
                                <ul>
                                  @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                  @endforeach
                                 </ul>
                            </div>
                        @endif

                        <form method="POST" action="{{url($definedCity->uri.'/'.$category->uri.'/post/save')}}" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Заголовок</label>
                                <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Ваш заголовок">
                            </div>
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Описание</label>
                                <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Добавить изображение</label>
                                <input type="file" name="images[]" multiple>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="hidden" name="anonymity" value="0">
                                        <input type="checkbox" value="1" name="anonymity"> Anonymity
                                    </label>
                                </div>
                            </div>


                            <input type="hidden" name="city[]" value="{{$city->id}}">
                            <input type="hidden" name="category[]" value="{{$category->id}}">
                            <button type="submit" class="btn btn-primary">Добавить</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection